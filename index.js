/* function toggle() {
  document.querySelector("h4").classList.toggle("active");
  document.querySelector(".item-wrapper").classList.toggle("active");
}

var chonH4 = document.querySelector("h4");
 */
var myArray = [];
function addNumber() {
  var number = document.getElementById("number").value * 1;
  myArray.push(number);
  document.getElementById("traMang").innerText = myArray;
  return myArray;
}
/* Bài 1: Tổng số dương
 */

/* function tinhtongSoDuong() {
  var result1 = 0;
  for (var index = 0; index < myArray.length; index++) {
    result1 += myArray[index];
  }
  document.getElementById("tongSoDuong").innerText = result1;
} */
function tinhtongSoDuong() {
  var listPositiveNumber = myArray

    .filter(function (value) {
      return value > 0;
    })
    .reduce(function (acc, curValue) {
      return (acc += curValue);
    });
  document.getElementById("tongSoDuong").innerText = listPositiveNumber;
}
/**
 * Bài 2: Đếm số dương
 */
/* function demSoDuong() {
  var result2 = 0;
  for (var index = 0; index < myArray.length; index++) {
    if (myArray[index] > 0) {
      result2++;
    }
  }
  document.getElementById("SoDuong").innerText = result2;
} */

function demSoDuong() {
  var listPositiveNumber = myArray.filter(function (value) {
    return value > 0;
  });

  document.getElementById("SoDuong").innerText = listPositiveNumber.length;
}

/**
 * Bài 3: Tìm số nhỏ nhất
 */

/* function timSoNhoNhat() {
  var minNumber = myArray[0];
  for (var index = 0; index < myArray.length; index++) {
    if (myArray[index] < minNumber) {
      minNumber = myArray[index];
    }
  }
  document.getElementById("soNhoNhat").innerText = minNumber;
} */

function timSoNhoNhat() {
  var minNumber = myArray.reduce(function (acc, curValue) {
    if (curValue < acc) {
      acc = curValue;
    }
    return acc;
  });
  document.getElementById("soNhoNhat").innerText = minNumber;
}

/**
 * Bài 4: Tìm số dương nhỏ nhất
 */
/* function timSoDuongNhoNhat() {
  var listPositiveNumber = [];
  for (var index = 1; index < myArray.length; index++) {
    if (myArray[index] > 0) {
      listPositiveNumber.push(myArray[index]);
    }
  }
  if (listPositiveNumber == 0) {
    var note = "Không có số dương";
  } else {
    var minNumber = listPositiveNumber[0];
    for (var index = 0; index < listPositiveNumber.length; index++) {
      if (listPositiveNumber[index] < minNumber) {
        minNumber = listPositiveNumber[index];
      }
    }
    note = `${minNumber}`;
  }
  document.getElementById("soDuongNhoNhat").innerText = note;
} */

function timSoDuongNhoNhat() {
  var listPositiveNumber = myArray.filter(function (value) {
    return value > 0;
  });
  var note = "";
  if (listPositiveNumber == 0) {
    note = "Không có số dương";
  } else {
    var minNumber = listPositiveNumber.reduce(function (acc, curValue) {
      if (curValue < acc) {
        acc = curValue;
      }
      return acc;
    });
    note = `${minNumber}`;
  }

  document.getElementById("soDuongNhoNhat").innerText = note;
}

/**
 * Bài 5: Tìm số chẵn cuối cùng
 */
function timSoChanCuoiCung() {
  var listEvenNumber = myArray.filter(function (value) {
    return value % 2 == 0;
  });
  var note = "";
  if (listEvenNumber[0] == undefined) {
    note = "Không có số chẵn "; // Có thể trả về -1 tại đây
  } else {
    var lastEvenNumber = listEvenNumber[listEvenNumber.length - 1];
    note = `${lastEvenNumber}`;
  }
  document.getElementById("soChanCuoiCung").innerText = note;
}

/**
 * Bài 6: Đổi chỗ
 */
function doiViTri() {
  var fisrtPosition = parseInt(document.getElementById("viTri1").value);
  var secondPosition = parseInt(document.getElementById("viTri2").value);
  var note = "";
  if (
    isNaN(fisrtPosition) ||
    isNaN(secondPosition) ||
    fisrtPosition > myArray.length ||
    secondPosition > myArray.length
  ) {
    note = "Vị trí được nhập không hợp lệ";
  } else {
    var fisrtNumber = myArray[fisrtPosition - 1];
    myArray[fisrtPosition - 1] = myArray[secondPosition - 1];
    myArray[secondPosition - 1] = fisrtNumber;
    note = `${myArray}`;
  }
  document.getElementById("traLaiMang").innerText = note;
}
/**
 * Bài 7: Sắp xếp tăng dần
 */
function sapXepTang() {
  var listIncreasing = myArray.sort(function (a, b) {
    return a - b;
  });
  document.getElementById("traDanhSachTang").innerText = listIncreasing;
}

/**
 * Bài 8: Tìm số nguyên tố đầu tiên
 */
function timSoNguyenToDauTien() {
  var listPositiveNumber = myArray.filter(function (value) {
    return value > 1;
  });
  var note = "";
  if (listPositiveNumber == 0) {
    note = "Không có số nguyên tố"; // Có thể trả về -1 tại đây
  } else {
    for (var index = 0; index < listPositiveNumber.length; index++) {
      var soLanChiaHetChoI = 0;
      for (var i = 2; i <= listPositiveNumber[index]; i++) {
        if (listPositiveNumber[index] % i == 0) {
          soLanChiaHetChoI++;
        }
      }
      if (soLanChiaHetChoI == 1) {
        note = `${listPositiveNumber[index]}`;
        break;
      }
    }
    if (note == "") {
      note = "Không có số nguyên tố"; // Có thể trả về -1 tại đây
    }
  }
  document.getElementById("traSoNguyenToDauTien").innerText = note;
}
/**
 * Bài 9: Đếm sô nguyên
 */
var myNewArray = [];
function addNumber9() {
  var number = document.getElementById("number9").value * 1;
  myNewArray.push(number);
  document.getElementById("traMang9").innerText = myNewArray;
  return myNewArray;
}
function demSoNguyen() {
  var listIntergerNumber = myNewArray.filter(function (value) {
    return Number.isInteger(value);
  });
  document.getElementById("traSoNguyen").innerText = listIntergerNumber.length;
}

/**
 * Bài 10: So sánh số lượng số âm và dương
 */
function soSanh() {
  var listPositiveNumber = myArray.filter(function (value) {
    return value > 0;
  });
  var listNegativeNumber = myArray.filter(function (value) {
    return value < 0;
  });
  var note = "";
  if (listNegativeNumber.length > listPositiveNumber.length) {
    note = "Số âm > Số dương";
  } else if (listNegativeNumber.length < listPositiveNumber.length) {
    note = "Số âm < Số dương";
  } else {
    note = "Số âm = Số dương";
  }
  document.getElementById("ketquasosanh").innerText = note;
}
